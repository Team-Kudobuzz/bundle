'use strict';

var express = require('express');
var passport = require('passport');
var auth = require('../auth.service');
var config = require('../../config/environment')
var router = express.Router();

router
  .get('/', function(req, res, next) {
	  	passport.authenticate('vimeo', {
	    failureRedirect: '/signup',
	    //callbackURL: config.vimeo.callbackURL+"?userId="+ req.query.userId ,
	    state : req.query.userId ,
	    session: false
	  })(req, res, next)
	})
  .get('/callback', passport.authenticate('vimeo', {
    failureRedirect: '/signup',
    session: false
  }), function(req, res) {
  	res.redirect('/vimeo');
  });

module.exports = router;
