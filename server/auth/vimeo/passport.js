var _ = require('lodash')
exports.setup = function (User, config) {
  var passport = require('passport');
  var VimeoStrategy = require('passport-vimeo-oauth2').Strategy;
  console.log("the config is --->", config )
  passport.use(new VimeoStrategy({
    clientID: config.vimeo.clientID,
    clientSecret: config.vimeo.clientSecret,
    passReqToCallback: true
    ,callbackURL: config.vimeo.callbackURL
  },
  function(req, token, tokenSecret, profile, done) {
    console.log("the req in vimeo ", req.query )
    console.log("the token in vimeo ", token )
    console.log("the tokenSecret in vimeo ", tokenSecret )
    console.log("the profile in vimeo ", profile )
    
    User.findOne({
        _id : req.query.state 
    }, function(err, user) {
      console.log("the user doing vimeo auth is -->",  user)
      if (err) {
        return done(err);
      }
      if(!user){
        var error = new Error("User not found") ;
        error.status = 401 ;
        return done( error )
      }
      var oldVimeo = _.merge({}, (user.vimeo || {}) )       
      user.vimeo = profile._json ;
      user.vimeo.status = oldVimeo.status || false ;
      user.vimeo.token = token ;

      //user.vimeo.tokenSecret = tokenSecret ;
      user.markModified('vimeo');
      user.save(function(err, nuser) {
        if (err) return done(err);
        console.log("error after saving vimeo details -->", err )
        console.log("user data saved --->", JSON.stringify( nuser ) )
        return done(err, user);
      })

    });
    }
  ));
};