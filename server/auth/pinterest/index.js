'use strict';

var express = require('express');
var passport = require('passport');
var auth = require('../auth.service');
var config = require('../../config/environment')
var router = express.Router();

router
.get('/', function(req, res, next) {
	passport.authenticate('pinterest', {
		failureRedirect: '/signup',
		scope: [
			'read_pubic',
			'write_public',
			'read_relationships',
			'write_relationships'
		],
	    //callbackURL: config.pinterest.callbackURL+"?userId="+ req.query.userId ,
	    state : req.query.userId ,
	    session: false
	})(req, res, next)
})
.get('/callback', passport.authenticate('pinterest', {
	failureRedirect: '/signup',
	session: false
}), function(req, res) {
	res.redirect('/pinterest');
});

module.exports = router;
