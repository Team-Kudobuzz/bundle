var _ = require('lodash')
exports.setup = function (User, config) {
  var passport = require('passport');
  var PinterestStrategy = require('passport-pinterest').OAuth2Strategy;
  console.log("the config is --->", config.pinterest )
  passport.use(new PinterestStrategy({
    clientID: config.pinterest.clientID,
    clientSecret: config.pinterest.clientSecret,
    passReqToCallback: true
    ,callbackURL: config.pinterest.callbackURL
  },
  function(req, token, tokenSecret, profile, done) {
    console.log("the req in pinterest ", req.query )
    console.log("the token in pinterest ", token )
    console.log("the tokenSecret in pinterest ", tokenSecret )
    console.log("the profile in pinterest ", profile )
    
    User.findOne({
        _id : req.query.state 
    }, function(err, user) {
      console.log("the user doing pinterest auth is -->",  user)
      if (err) {
        return done(err);
      }
      if(!user){
        var error = new Error("User not found") ;
        error.status = 401 ;
        return done( error )
      }
      var oldPinterest = _.merge({}, (user.pinterest || {}) )       
      user.pinterest = profile._json ;
      user.pinterest.status = oldPinterest.status || false ;
      user.pinterest.token = token ;

      //user.pinterest.tokenSecret = tokenSecret ;
      user.markModified('pinterest');
      user.save(function(err, nuser) {
        if (err) return done(err);
        console.log("error after saving pinterest details -->", err )
        console.log("user data saved --->", JSON.stringify( nuser ) )
        return done(err, user);
      })

    });
    }
  ));
};