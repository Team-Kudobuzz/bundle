var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;
var request = require('request');
var _ = require('lodash')

exports.setup = function (User, config) {
  console.log("the url is -->", config.facebook.callbackURL + "/" )
  passport.use(new FacebookStrategy({
    clientID: config.facebook.clientID,
    clientSecret: config.facebook.clientSecret,
    callbackURL: config.facebook.callbackURL + "/",
    profileFields : ['id','name','accounts{about,id,name,affiliation,access_token,picture}','birthday','link','email','picture{url}','gender','devices'] ,
    profileURL : 'https://graph.facebook.com/v2.5/me' ,
    passReqToCallback: true
  },
  function(req, accessToken, refreshToken, profile, done) {
    console.log("the req.query ------", req.query )
    console.log("the accessToken -----------", accessToken )
    console.log("refreshToken ----------", refreshToken )
    console.log("profile ----------", profile )      


    User.findOne({
      _id : req.query.state 
    },
    function(err, user) {
      if (err) {
        return done(err);
      }
      var oldFacebookPages = ((_.merge({}, (user.facebook||{}).accounts)||{}).data || [] );
      // console.log("the oldFacebookPages -->", oldFacebookPages )
      user.facebook = _.merge({}, profile._json );
      //console.log("the new facebook -->", user.facebook.accounts )

      // var oldFacebookPages = ((user.facebook||{}).accounts || {});
      user.facebook =_.merge(user.facebook, profile._json)
      //console.log("thr user.facebook ---->", user.facebook )

      user.facebook.accounts = {}

      //console.log("the profile._json ---> ", profile._json.accounts )
      var newPages = ((profile._json.accounts || {}).data || [] )
      console.log("the looper ---> ", newPages )
      
      newPages.map(function(newPage, index) {        
        newPage.isActive = false ;
        newPage.displayCount = 10 ;
        user.facebook.accounts[newPage.id] = _.merge( newPage , (oldFacebookPages||{})[newPage.id] || {} )

      }) 

      console.log("the user ---------------------> ", JSON.stringify(user) )      

      user.markModified('facebook' );
      user.markModified('facebook.accounts' );
      user.save(function(err, nuser) {
        if (err) return done(err);
        console.log("error after saving facebook details -->", err )
        console.log("user data saved --->", ( nuser._id ) )
        
        console.log("user data saved --->", JSON.stringify( nuser ) )
        return done(err, nuser );       
      });
    })
  }
));
};