'use strict';

var express = require('express');
var passport = require('passport');
var auth = require('../auth.service');
var config = require('../../config/environment')
var router = express.Router();
console.log("the config.callbackURL", config.facebook.callbackURL )
router
.get('/', function(req, res, next) {
	passport.authenticate('facebook', {
		scope: ['email', 'user_about_me', 'pages_show_list', 'manage_pages'],
		failureRedirect: '/signup', 
		state : req.query.userId,   
		session: false
	})(req, res, next);
} )

.get('/callback/', passport.authenticate('facebook', {
	failureRedirect: '/signup',
	session: false
  }), function(req, res) {
  	res.redirect('/facebook');
  });

module.exports = router;