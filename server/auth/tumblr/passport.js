var _ = require('lodash')
exports.setup = function (User, config) {
  var passport = require('passport');
  var TumblrStrategy = require('passport-tumblr').Strategy;

  passport.use(new TumblrStrategy({
    consumerKey: config.tumblr.clientID,
    consumerSecret: config.tumblr.clientSecret,
    passReqToCallback: true
    ,callbackURL: config.tumblr.callbackURL
  },
  function(req, token, tokenSecret, profile, done) {
    console.log("the req in tumblr ", req.query )
    console.log("the token in tumblr ", token )
    console.log("the tokenSecret in tumblr ", tokenSecret )
    console.log("the profile in tumblr ", profile )

    User.findOne({
        _id : req.query.userId 
    }, function(err, user) {
      console.log("the user doing tumblr auth is -->",  user)
      if (err) {
        return done(err);
      }
      if(!user){
        var error = new Error("User not found") ;
        error.status = 401 ;
        return done( error )
      }

      var oldTumblr = _.merge({}, (user.tumblr || {}) )       
      user.tumblr = ((profile._json || {}).response).user ;
      user.tumblr.token = token ;      
      user.tumblr.status = oldTumblr.status || false ; 


      user.markModified('tumblr');
      user.save(function(err, nuser) {
        if (err) return done(err);
        console.log("error after saving tumblr details -->", err )
        console.log("user data saved --->", JSON.stringify( nuser ) )
        return done(err, user);
      })

    });
    }
  ));
};