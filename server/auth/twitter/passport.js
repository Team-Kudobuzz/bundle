var _ = require('lodash')
exports.setup = function (User, config) {
  var passport = require('passport');
  var TwitterStrategy = require('passport-twitter').Strategy;

  passport.use(new TwitterStrategy({
    consumerKey: config.twitter.clientID,
    consumerSecret: config.twitter.clientSecret,
    passReqToCallback: true
    /*callbackURL: config.twitter.callbackURL*/
  },
  function(req, token, tokenSecret, profile, done) {
    console.log("the req in twitter ", req.query )
    console.log("the token in twitter ", token )
    console.log("the tokenSecret in twitter ", tokenSecret )
    console.log("the profile in twitter ", profile )
    // return;
    User.findOne({
      // 'twitter.id_str': profile.id
       _id : req.query.userId 
    }, function(err, user) {
      console.log("the user doing twitter auth is -->",  user)
      if (err) {
        return done(err);
      }
      if(!user){
        var error = new Error("") ;
        error.status = 401 ;
        return done( error )
      }

      var oldTwitter = _.merge({}, (user.twitter || {}) )       
      user.twitter = profile._json ;      
      user.twitter.status = oldTwitter.status || false ;
      
      user.twitter.token = token ;

      user.markModified('twitter' );
      user.save(function(err, nuser) {
        if (err) return done(err);
        console.log("error after saving facebook details -->", err )
        console.log("user data saved --->", JSON.stringify( nuser ) )
        return done(err, user);
      })

    /*  if (!user) {
        user = new User({
          name: profile.displayName,
          username: profile.username,
          role: 'user',
          provider: 'twitter',
          twitter: profile._json
        });
        user.save(function(err) {
          if (err) return done(err);
          return done(err, user);
        });
      } else {
        return done(err, user);
      }*/
    });
    }
  ));
};