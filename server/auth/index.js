'use strict';

var express = require('express');
var passport = require('passport');
var config = require('../config/environment');
var User = require('../api/user/user.model');
var auth = require('./auth.service')

// Passport Configuration
require('./local/passport').setup(User, config);
require('./facebook/passport').setup(User, config);
require('./google/passport').setup(User, config);
require('./twitter/passport').setup(User, config);
require('./instagram/passport').setup(User, config);
require('./tumblr/passport').setup(User, config);
require('./vimeo/passport').setup(User, config);
// require('./pinterest/passport').setup(User, config);
var router = express.Router();

router.use('/local', require('./local'));
router.use('/facebook', require('./facebook'));
router.use('/twitter', require('./twitter'));
router.use('/google', require('./google'));
router.use('/instagram', require('./instagram'));
router.use('/tumblr', require('./tumblr'));
router.use('/vimeo', require('./vimeo'));
// router.use('/pinterest', require('./pinterest'));
module.exports = router;