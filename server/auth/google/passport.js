var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var _ = require('lodash')

exports.setup = function (User, config) {
  passport.use(new GoogleStrategy({
    clientID: config.google.clientID,
    clientSecret: config.google.clientSecret,
    callbackURL: config.google.callbackURL,
    passReqToCallback: true
  },
  function(req, accessToken, refreshToken, profile, done) {

   console.log("the req in google ", req.query )
   console.log("the accessToken in google ", accessToken )
   console.log("the refreshToken in google ", refreshToken )
   console.log("the profile in google ", profile )

   User.findOne({
     _id : req.query.state 
   }, function(err, user) {
    // console.log("the user doing twitter auth is -->",  user.google.refreshToken )
     if (err) {
      return done(err);
    }
    if(!user){
      var error = new Error("") ;
      error.status = 401 ;
      return done( error ) 
    }

      var oldGoogle = _.merge({}, (user.google || {}) )       
      user.google = profile._json ;
      
      user.google.refreshToken = refreshToken ;
      user.google.accessToken = accessToken ;

      user.google.status = oldGoogle.status || false ; 

      user.markModified('google' );
      user.save(function(err, nuser) {
        if (err) return done(err);
        console.log("error after saving google details -->", err )
        console.log("user data saved --->", JSON.stringify( nuser ) )
        return done(err, nuser);
      })/*
      if (!user) {
        user = new User({
          name: profile.displayName,
          email: profile.emails[0].value,
          role: 'user',
          username: profile.username,
          provider: 'google',
          google: profile._json
        });
        user.save(function(err) {
          if (err) done(err);
          return done(err, user);
        });
      } else {
        return done(err, user);
      }*/
    });
}
));
};
