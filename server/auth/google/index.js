'use strict';

var express = require('express');
var passport = require('passport');
var auth = require('../auth.service');

var router = express.Router();

router
.get('/', 
  function(req, res, next) {
    passport.authenticate('google', {
      scope: [
      'https://www.googleapis.com/auth/userinfo.profile',
      'https://www.googleapis.com/auth/userinfo.email'
    ],
    failureRedirect: '/signup', 
    state : req.query.userId,
    accessType : 'offline',
    approvalPrompt: 'force'
    //,   
    // session: false
    })(req, res, next);
  } 
  /*passport.authenticate('google', {
    failureRedirect: '/signup',
    scope: [
      'https://www.googleapis.com/auth/userinfo.profile',
      'https://www.googleapis.com/auth/userinfo.email'
    ],
    session: false
  })*/)

.get('/callback', passport.authenticate('google', {
  failureRedirect: '/signup',
  session: false
}), function(req, res) {
    res.redirect('/googleplus');
  } );

module.exports = router;