var _ = require('lodash')

exports.setup = function (User, config) {
  var passport = require('passport');
  var InstagramStrategy = require('passport-instagram').Strategy;

  passport.use(new InstagramStrategy({
    clientID: config.instagram.clientID,
    clientSecret: config.instagram.clientSecret,
    passReqToCallback: true,
    callbackURL: config.instagram.callbackURL
  },
  function(req, token, tokenSecret, profile, done) {
    console.log("the req in instagram ", req.query )
    console.log("the token in instagram ", token )
    console.log("the tokenSecret in instagram ", tokenSecret )
    console.log("the profile in instagram ", profile )

    User.findOne({  
      // 'instagram.id_str': profile.id
       _id : req.query.state 
    }, function(err, user) {
      console.log("the user doing instagram auth is -->",  user)
      if (err) {
        return done(err);
      }
      if(!user){
        var error = new Error("") ;
        error.status = 401 ;
        return done( error )
      }
      
      var oldInstagram = _.merge({}, (user.instagram || {}) )       
      user.instagram = profile._json ;
      user.instagram.token = token ;      
      user.instagram.status = oldInstagram.status || false ; 
         
      user.markModified('instagram' );
      user.save(function(err, nuser) {
        if (err) return done(err);
        console.log("error after saving instagram details -->", err )
        console.log("user data saved --->", JSON.stringify( nuser ) )
        return done(err, user);
      })

    });
    }
  ));
};