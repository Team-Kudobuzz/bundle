'use strict';

var User = require('./user.model');
var _ = require('lodash');
var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');

var validationError = function(res, err) {
  return res.status(422).json(err);
};

/**
 * Get list of users
 * restriction: 'admin'
 */
 exports.index = function(req, res) {
  User.find({}, '-salt -hashedPassword', function (err, users) {
    if (err) return res.status(500).json(err);
    res.status(200).json(users);
  });
};

/**
 * Creates a new user
 */
 exports.create = function (req, res, next) {
  var newUser = new User(req.body);
  newUser.provider = 'local';
  newUser.role = 'user';
  newUser.save(function(err, user) {
    if (err) return validationError(res, err);
    var token = jwt.sign({_id: user._id }, config.secrets.session, { expiresInMinutes: 60*5 });
    res.status(200).json({ token: token });
  });
};

/**
 * Get a single user
 */
 exports.show = function (req, res, next) {
  var userId = req.params.id;

  User.findById(userId, function (err, user) {
    if (err) return next(err);
    if (!user) return res.status(401).json({status:false, message : 'Not Found'});
    res.status(200).json(user.profile);
  });
};

/**
 * Deletes a user
 * restriction: 'admin'
 */
 exports.destroy = function(req, res) {
  User.findByIdAndRemove(req.params.id, function(err, user) {
    if (err) return res.status(500).json(err);
    return res.status(204).json({status:true, message:'destroyed'});
  });
};

/**
 * Change a users password
 */
 exports.changePassword = function(req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  User.findById(userId, function (err, user) {
    if (user.authenticate(oldPass)) {
      user.password = newPass;
      user.save(function(err) {
        if (err) return validationError(res, err);
        res.status(200).json({status:true, message:'password changed'});
      });
    } else {
      res.status(403).send(new Error('Not Found'));
    }
  });
};

/**
 * Change a users password
 *///:userId/:platform/:platformId/activate
 exports.activate = function(req, res, next) {
  var userId = req.params.userId, platform = req.params.platform, platformId = req.params.platformId , flag = ( req.query.flag == 1);
  console.log("the real flag is --->", req.query.flag )
    console.log("the real platform is --->", platform )
      console.log("the real platformId is --->", platformId )
  
  User.findById(userId, function (err, user) {
    if (err) return res.status(500).json(err);
    if(!user) return res.status(401).json(new Error('No user found with that id'))
      switch(platform){
        case 'facebook' :
          user.facebook.accounts[platformId]. isActive = flag ;
        default :
        console.log("the other platforms havent been done yet ");        
      }

      user.markModified( platform );
      user.save(function(err, nUser) {
        if (err) return validationError(res, err);
        if(!nUser){
          return res.status(401).json(new Error('after editing '+ ' with channel on '+ platform + ' , couldnt save data'))
        }
        console.log(flag, ">>the nUser after update -->", JSON.stringify (nUser) )
        return res.status(200).json( nUser )
      })



    });
};

/**
 * updateChannel 
 */
 exports.updateChannel = function(req, res, next) {
  var userId = req.params.id, platform = req.params.platform, channel = req.params.channel, body = ( req.body );
  console.log("the req.body is --->", req.body )

  User.findById(userId, function (err, user) {
    if (err) return res.status(500).json(err);
    if(!user) return res.status(401).json(new Error('No user found with that id'))
      var plat  = user.channels[platform] ;
    if(!plat) return res.status(401).json(new Error( platform +' has not been activated'))
      var platyHit = false ;
    plat.map(function(platy, index) {

      if(platy.id == channel ) {
        platyHit = true ; 
        console.log("the platy ", platy )
        var after = _.extend(platy, req.body );
        console.log("the platy after ", platy )
        console.log("the after  ", after )
      }
      return platy ;
    })

    if(platyHit){
      user.markModified('channels.'+platform );
      user.save(function(err, nUser) {
        if (err) return validationError(res, err);
        if(!nUser){
          return res.status(401).json(new Error('after editing '+ ' with channel on '+ platform + ' , couldnt save data'))
        }
        console.log(">>the nUser after update -->", JSON.stringify (nUser) )
        return res.status(200).json( nUser )
      })
    }

    
  });
};

/**
 * Get my info
 */
 exports.me = function(req, res, next) {
  var userId = req.user._id;
  User.findOne({
    _id: userId
  }, '-salt -hashedPassword', function(err, user) { // don't ever give out the password or salt
  if (err) return next(err);
  if (!user) return res.status(401).json(new Error('Not authenticated'));
  res.status(200).json(user);
});
};

/**
 * Authentication callback
 */
 exports.authCallback = function(req, res, next) {
  res.redirect('/');
};
