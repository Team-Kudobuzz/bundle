var socialPluginFacebookTemplate = 
[	'<div style="width:150px; float:left;	',
'display: block;',
'padding: 4px;',
'margin: 5px 0px 5px 5px;',
'line-height: 1.42857143;',
'background-color: #fff;',
'border: 1px solid #ddd;',
'border-radius: 4px;',
'-webkit-transition: all .2s ease-in-out;',
'transition: all .2s ease-in-out;" >',
'<div style="height:100px;',
'	background:#eee url(\'{{post-image}}\') no-repeat;background-position: center" ></div>',
'<div style="padding:0">',
'	<h5 style="margin:5px 0 0px 0px" >',
'		<div style="width:15px;height:15px;',
'			display:inline-block;background:#ccc url(\'{{author-image}}\') no-repeat;background-position: center"',
'		"></div>',	
'		<span> {{name}} </span>',
'	</h5>',
'	<p style="margin:0px;height:100px;overflow:hidden;word-break:break-word">',
'		<i style="margin:0px;">',
'			<small style="margin:0px;">{{message}}',
'			</small>',
'		</i>',
'	</p>',
'	<p style="background-color:#eef;padding:0px;margin:0px"></p>',
'</div>'
].join('\n')


var trimMessage = function(message) {
	var len = 100 
	return message.length > len ? message.substring(0, len-6 ) +" . . ." : message ;
};

var loadPosts = function(postsObj, parent ) {
	var posts = postsObj.data || [] ;
	console.log("the posts --> ", posts)
	console.log("the posts --> parent ", parent.length )
	posts.map(function(post, index) {
		parent
		// .html('')
		.append( 
			socialPluginFacebookTemplate
			.replace('{{name}}' , post.from.name )
			.replace('{{message}}', trimMessage(post.message || post.link  || post.picture ) )
			.replace('{{post-image}}', post.picture || post.from.picture.data.url )
			.replace('{{call-toaction}}', post.link)
			.replace('{{author-image}}', post.from.picture.data.url )
			)	
	})
};

for( var id in socialPlugin_me.facebook.accounts){
	var page = socialPlugin_me.facebook.accounts[id] ;
	console.log("the id is ", id )
	console.log("the div is ", $('#socialPlugin-facebook-' + id).length )
	console.log("the me is " , socialPlugin_me.facebook.accounts[id] )
	$('#socialPlugin-facebook-' + id).css({
		//'background-color':'red',
		'width':'800px',
		'overflow': 'hidden'});
	if(socialPlugin_me.facebook.accounts[id].isActive && (socialPluginJquery('#socialPlugin-facebook-' + id ).length) ){
		
		var path = "https://graph.facebook.com/v2.5/"+
			id +
			"/feed?"+
			//"fields=message,created_time,object_id,from{link,name,picture}&include_hidden=false"+
			"fields=message,created_time,object_id,from{link,name,picture},caption,picture,link,source&include_hidden=true"+
			"&access_token=" + page.access_token ;
		socialPluginJquery
		.get( path )
		.success(function(posts) {
			console.log("the posts are ", posts )
			loadPosts(posts, $('#socialPlugin-facebook-' + this.pageId) )
		}.bind({pageId:id}) )
		.error(function(err) {
			console.log("the err is ----> ", err )
			$('#socialPlugin-facebook-' + id).hide();
		}.bind({pageId:id}) )

	}
}
console.log("facebook enabled" )