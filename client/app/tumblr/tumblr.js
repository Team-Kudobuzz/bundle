;(function(){
'use strict';
angular
  .module('socialPluginApp')
  .config(Configuration);

  /* @ngInject */
  function Configuration($routeProvider) {
    // Tumblr state routing
    $routeProvider
      .when('/tumblr', {
        templateUrl: 'app/tumblr/tumblr.html',
        controller: 'TumblrCtrl',
        athenticate:true
      });
  }

}).call(this);
