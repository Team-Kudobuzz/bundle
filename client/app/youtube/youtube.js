;(function(){
'use strict';
angular
  .module('socialPluginApp')
  .config(Configuration);

  /* @ngInject */
  function Configuration($routeProvider) {
    // Youtube state routing
    $routeProvider
      .when('/youtube', {
        templateUrl: 'app/youtube/youtube.html',
        controller: 'YoutubeCtrl'
      });
  }

}).call(this);
