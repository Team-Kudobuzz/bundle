;(function(){
'use strict';
angular
  .module('socialPluginApp')
  .config(Configuration);

  /* @ngInject */
  function Configuration($routeProvider) {
    // Googleplus state routing
    $routeProvider
      .when('/googleplus', {
        templateUrl: 'app/googleplus/googleplus.html',
        controller: 'GoogleplusCtrl',
         authenticate: true
      });
  }

}).call(this);
