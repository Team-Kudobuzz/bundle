'use strict';

describe('Controller: GoogleplusCtrl', function () {

  // load the controller's module
  beforeEach(module('socialPluginApp'));

  var GoogleplusCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    GoogleplusCtrl = $controller('GoogleplusCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
  });
});
