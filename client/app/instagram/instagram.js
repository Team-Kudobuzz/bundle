;(function(){
'use strict';
angular
  .module('socialPluginApp')
  .config(Configuration);

  /* @ngInject */
  function Configuration($routeProvider) {
    // Instagram state routing
    $routeProvider
      .when('/instagram', {
        templateUrl: 'app/instagram/instagram.html',
        controller: 'InstagramCtrl',
        authenticate: true
      });
  }

}).call(this);
