;(function(){
'use strict';
angular
  .module('socialPluginApp')
  .config(Configuration);

  /* @ngInject */
  function Configuration($routeProvider) {
    // Twitter state routing
    $routeProvider
      .when('/twitter', {
        templateUrl: 'app/twitter/twitter.html',
        controller: 'TwitterCtrl',
        authenticate: true
      });
  }

}).call(this);
