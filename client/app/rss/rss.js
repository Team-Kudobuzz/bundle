;(function(){
'use strict';
angular
  .module('socialPluginApp')
  .config(Configuration);

  /* @ngInject */
  function Configuration($routeProvider) {
    // Rss state routing
    $routeProvider
      .when('/rss', {
        templateUrl: 'app/rss/rss.html',
        controller: 'RssCtrl',
        athenticate : true
      });
  }

}).call(this);
