'use strict';

describe('Controller: RssCtrl', function () {

  // load the controller's module
  beforeEach(module('socialPluginApp'));

  var RssCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RssCtrl = $controller('RssCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
  });
});
