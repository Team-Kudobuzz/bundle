'use strict';

describe('Controller: VimeoCtrl', function () {

  // load the controller's module
  beforeEach(module('socialPluginApp'));

  var VimeoCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    VimeoCtrl = $controller('VimeoCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
  });
});
