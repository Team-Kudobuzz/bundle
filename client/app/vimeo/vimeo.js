;(function(){
'use strict';
angular
  .module('socialPluginApp')
  .config(Configuration);

  /* @ngInject */
  function Configuration($routeProvider) {
    // Vimeo state routing
    $routeProvider
      .when('/vimeo', {
        templateUrl: 'app/vimeo/vimeo.html',
        controller: 'VimeoController'
      });
  }

}).call(this);
