;(function(){
'use strict';
angular
  .module('socialPluginApp')
  .config(Configuration);

  /* @ngInject */
  function Configuration($routeProvider) {
    // Facebook state routing
    $routeProvider
      .when('/facebook', {
        templateUrl: 'app/facebook/facebook.html',
        controller: 'FacebookCtrl',
        authenticate: true
      });
  }

}).call(this);
