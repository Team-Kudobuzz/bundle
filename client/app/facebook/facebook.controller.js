;(function(){
	'use strict';
	angular
	.module('socialPluginApp')
	.controller('FacebookCtrl', FacebookCtrl);

	/* @ngInject */
	function FacebookCtrl($scope, User, $timeout, Auth, $cookies, $http) {
    // Controller Logic
    $scope.currentUser = Auth.getCurrentUser();
    console.log("the currentChannel ", localStorage.getItem('currentChannelFacebook' ) )
    $scope.currentChannel =  JSON.parse( localStorage.getItem('currentChannelFacebook' ) || null ) || {} ;    	
    
    User.get()
    .$promise.then(function(user) {
    	if(user){
    		console.log("the user is ----> ", user )
            for(var pageId  in user.facebook.accounts ){
                if (pageId == $scope.currentChannel.id ) {
                    $scope.currentChannel =  user.facebook.accounts[pageId] ;
                    localStorage.setItem('currentChannelFacebook' , JSON.stringify(user.facebook.accounts[pageId]) );
                };
            }

            $timeout(function() {
                console.log("the user in timeout  is ----> ", user )
             Auth.setCurrentUser(user);
             $scope.currentUser = Auth.getCurrentUser();
         })    			
        }
    })

    $scope.clickOnChannel = function(channel) {
        $scope.currentChannel = channel ;
    	//angular.merge(	$scope.currentChannel , channel );
    	localStorage.setItem('currentChannelFacebook' , JSON.stringify(channel) );
    	console.log("the currentChannel ", $scope.currentChannel )
    };
    $scope.hasFacebookAccounts = function() {
        return Object.keys(Auth.getCurrentUser().facebook.accounts).length 
    };
    $scope.activate = function(channel, flag) {
    	console.log("the activated flag", flag )
        $http.put("http://localhost:9000/api/users/"+ $scope.currentUser._id+"/facebook/"+ channel.id + "/activate?flag="+flag )
        .then(function(nPage) {
            console.log("the this in $http ", this )
            console.log("the success page ", nPage );                           
            $timeout(function() {
                console.log("the this in last timeout ", this )
                angular.merge( this.channel , nPage.data.facebook.accounts[this.channel.id] )
                    // $scope.currentUser.channels.facebook[this.index].isActive = 
                }.bind(this))
        }.bind({channel : channel}), function(err) {
            console.log("the error after activate", err)
        }.bind({channel:channel}))

    	/*$scope.currentUser.channels.facebook.map(function(page, index) {
    		$timeout(function() {
    			console.log("the this in timeout ", this )
    			if(page.id == channel.id){
    				page.isLoading = true ;	
    				$http.put("http://localhost:9000/api/users/"+ $scope.currentUser._id+"/facebook/"+ page.id + "/activate?flag="+flag )
    				.then(function(nPage) {
    					console.log("the this in $http ", this )
    					console.log("the success page ", nPage );    						
    					$timeout(function() {
    						console.log("the this in last timeout ", this )
    						page.isLoading = false ;	
    						angular.merge(this.page, nPage.data.channels.facebook[this.index] )
    						// $scope.currentUser.channels.facebook[this.index].isActive = 
    						}.bind(this))
    				}.bind(this), function(err) {
    					console.log("the error after activate", err)
    				})
    			} 
    		}.bind({page:page, index: index}))
    		return page ;
    		console.log("the channel is active ", page.isActive )
    	})*/
};

$scope.submit = function  () {
   console.log("the currentChannel is ", $scope.currentChannel )
   $http.put("http://localhost:9000/api/users/"+ $scope.currentUser._id+"/facebook/"+ $scope.currentChannel.id + "/update", $scope.currentChannel )
   .then(function(nPage) {
      console.log("the this in $http ", this );
      console.log("the success page ", nPage );    		    			
  }.bind(this), function(err) {
      console.log("the error after activate", err)
  })
}

}


}).call(this);