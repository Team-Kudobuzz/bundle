'use strict';

describe('Controller: PinterestCtrl', function () {

  // load the controller's module
  beforeEach(module('socialPluginApp'));

  var PinterestCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PinterestCtrl = $controller('PinterestCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
  });
});
