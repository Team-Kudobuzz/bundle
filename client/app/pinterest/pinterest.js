;(function(){
'use strict';
angular
  .module('socialPluginApp')
  .config(Configuration);

  /* @ngInject */
  function Configuration($routeProvider) {
    // Pinterest state routing
    $routeProvider
      .when('/pinterest', {
        templateUrl: 'app/pinterest/pinterest.html',
        controller: 'PinterestController'
      });
  }

}).call(this);
