;(function(){
'use strict';
angular
  .module('socialPluginApp', [
    
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'btford.socket-io',
  'ui.bootstrap',
  'ngModal'

  ])
  .config(Configuration) 
  .factory('authInterceptor', authInterceptor)
  .run(run);

  /* @ngInject */
  function authInterceptor($rootScope, $q, $cookies, $location) {
    var instance = {
      request: request,
      responseError: responseError
    };

    return instance;

    // Add authorization token to headers
    function request(config) {
      config.headers = config.headers || {};
      if ($cookies.get('token')) {
        config.headers.Authorization = 'Bearer ' + $cookies.get('token');
      }
      return config;
    }

    // Intercept 401s and redirect you to login
    function responseError(response) {
      if (response.status === 401) {
        $location.path('/login');
        // remove any stale tokens
        $cookies.remove('token');
        return $q.reject(response);
      } else {
        return $q.reject(response);
      }
    }
  }

  /* @ngInject */
  function run($rootScope, $location, Auth) {
    // Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$routeChangeStart', function (event, next) {
      Auth.isLoggedInAsync(function(loggedIn) {
        if (next.authenticate && !loggedIn) {
          $location.path('/login');
        }
      });
    });
  }
  
  /* @ngInject */
  function Configuration($routeProvider, $locationProvider, $httpProvider) {
    $routeProvider
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider.html5Mode(true);
    $httpProvider.interceptors.push('authInterceptor');
  }
   
}).call(this);