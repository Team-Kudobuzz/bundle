;(function(){
'use strict';
angular
  .module('socialPluginApp')
  .controller('MainCtrl', MainCtrl);
  
  /* @ngInject */
  function MainCtrl($scope, $http, $cookies, socket, Auth, $window) {

    $scope.currentUser = Auth.getCurrentUser();
    console.log("the currentUser --->", $scope.currentUser )
    $scope.currentUser.token = $cookies.get('token') ;

    $scope.facebookModal = false ;
    $scope.twitterModal = false ;
    $scope.youtubeModal = false ;

    $scope.modal = {
      show : false,
      title : "",
      which : {
        facebook : false ,
        twitter  : false ,
        youtube : false 
      }
    }

    $scope.showModal = function(platform) {
      $scope.modal.title = platform ;
      $scope.modal.show = platform ;  
      console.log("in showModal", $scope.modal ) 
      $scope.modal.which[platform.toLowerCase()] = true ; 
    };
    
  $scope.loginOauth = function(provider) {
    var path = '/auth/' + provider + "?userId=" + $scope.currentUser._id ;
    console.log("the apth in  loginOauth ", path )
      $window.location.href = path ;
    };
  }  
  
}).call(this);
